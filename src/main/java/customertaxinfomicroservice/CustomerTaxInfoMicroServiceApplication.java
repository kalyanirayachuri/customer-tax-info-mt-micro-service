package customertaxinfomicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerTaxInfoMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerTaxInfoMicroServiceApplication.class, args);
	}

}

